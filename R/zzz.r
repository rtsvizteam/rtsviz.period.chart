.onLoad <- function(libname, pkgname) {
	# register rtsviz plugins, do not overwrite if already registered
	rtsviz::register.rtsviz.plugin('period.chart',
		'Period Chart','Period Chart Tool',
		'Examine one year performance for selected date',
		'Examples', period.chart.ui, period.chart.server
		,overwrite=FALSE)
		
		
	invisible()
}



