###############################################################################
#' Period Chart shiny app Server object
#'
#' Create the 'period.chart' shiny Server
#'
#' @param input list of input objects for shiny app
#' @param output list of output objects for shiny app
#' @param session session for shiny app
#'
#' @return shiny Server object
#'
#' @export
###############################################################################
period.chart.server = function(input, output, session) {
	#--------------------------
	# define variables
	#--------------------------
	session = session
	app.id = "period.chart.applicationID"
	
	
	
	
	# Create an environment for storing data
	symbols.data.cache = new.env()


	#--------------------------
	# pre-made functionality
	#--------------------------	
	# non reactive
	valid = rtsviz::shiny.valid()
	getLogs = rtsviz::shiny.getLogs() 

	# reactive
	getStocks = reactive(rtsviz::shiny.getStocks())
	getData = reactive(rtsviz::shiny.getData(p.end=0.6))

	# get data
  	getData1 <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData()) )) return(NULL)
		
		symbols = getStocks()
		ticker = paste0(input$dataSource,symbols[1])
		temp = data[[ ticker ]]
				
		rtsviz::make.stock(temp)
	})
	
	
    #*****************************************************************
    # First run message
    #******************************************************************    		
	output$appFirstHelp = renderUI(rtsviz::shiny.appFirstHelp())

	
    #*****************************************************************
    # Update plot(s) and table(s)
    #*****************************************************************
	# Subsample data around reference date
	getSelectedData <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)

		period.chart.data(getStocks(), data, input$date, input$months, input$frequency)
	})

	
	# Generate a plot	
	output$strategyPlot <- renderPlot({ 
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)				
				
		logs = getLogs('Plot:', p.start=0.6, p.end=1)
		logs$log('Start', percent=0/100)  
				
		data = getSelectedData()
		period.chart.makeStockPlot(getStocks(), data$data, x.highlight = data$after)
		
		logs$log('End', percent=100/100)		
	})	
	
	
	# Generate table
	getStrategyTable <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
		
		data = getSelectedData()		
		period.chart.makeStatsTable(getStocks(), data)		
	})
	
	
	# Generate a table
	output$strategyTableHTML <- reactive({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
				
		df1 = getStrategyTable()
		kableExtra::kable_styling(
			knitr::kable(df1, "html"),
			c("striped", "hover", "condensed", "responsive"), 
			full_width = FALSE,
			position = 'left'
		)
	})
	
	output$copy2clip <- renderUI({
		# control if any action needs to be done
		if(is.null( (data = getData1()) )) return(NULL)
		
		df1 = getStrategyTable()		
		rclipboard::rclipButton("copy2clipbtn", "Copy", table2str(df1, row.names = TRUE), icon("clipboard"))
	})
	
    #*****************************************************************
    # Download
    #******************************************************************    
    # Download pdf report
	output$downloadReport <- downloadHandler(
    	filename = 'report.pdf',
    	content = function(file) {
			# control if any action needs to be done
			if(is.null( (data = getData1()) )) return(write.file('No data loaded.', file=file))
			
    		pdf(file = file, width=8.5, height=11)
      			
			data = getSelectedData()
			period.chart.makeStockPlot(getStocks(), data$data, x.highlight = data$after)
			
			df = getStrategyTable()
				
			grid::grid.newpage()
			g2 =  gridExtra::tableGrob(df)		
			g2$widths = grid::unit(rep(1/ncol(g2), ncol(g2)), "npc")
			grid::grid.draw(g2)
      		
		    dev.off()
    	}
	)	
		
	# Download csv data
	output$downloadData <- downloadHandler(
    	filename = 'data.csv',
    	content = function(file) {
			# control if any action needs to be done
			if(is.null( (data = getData1()) )) return(write.file('No data loaded.', file=file))
			
      		utils::write.csv(getStrategyTable(), file=file, row.names = TRUE)
    	}
  	)	

	
	# Download Rmd template
	output$downloadRmd <- downloadHandler(
    	filename = 'period.chart.Rmd',
    	content = function(file) {
			# control if any action needs to be done
			if(is.null( (data = getData1()) )) return(write.file('No data loaded.', file=file))
			
			s = read.file(system.file('template', 'period.chart.Rmd', package='rtsviz.period.chart'))
			s = gsub('<source>',input$dataSource, 
				gsub('<symbol>',getStocks(), 
				gsub('<frequency>',input$frequency, 				
				gsub('<reference.date>',input$date, 
				gsub('<months>',input$months, 
				s)))))
			write.file(s, file=file)
    	}
  	)	
	
	
}