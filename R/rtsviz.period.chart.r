#' @title `rtsviz.period.chart` - Sample plugin for the `rtsviz` package.
#' 
#' @description This user friendly shiny application allows to interactively set a reference date 
#'	and show n months prior and 12-n months after. User can specify 
#'	day, week or monthly data frequency for the visualization.
#'
#' 
#' @examples
#' \dontrun{ 
#'	library(quantmod)
#'		
#'	# enable persistent time series data storage with `rtsdata` package
#'	library(rtsdata)
#'	
#'	# load `rtsviz.period.chart` package
#'	library(rtsviz.period.chart)
#'		
#'	# start Shiny interface
#'	launchApp()
#'		
#'		
#'	# ---------------
#'	# start Period Chart app in the `rtsviz` interface
#'	# please note this package registers 'period.chart' app as `rtsviz` plugin
#'	# ---------------	
#'	# load `rtsviz` package; will **overwrite** `launchApp` function
#'	library(rtsviz)
#'		
#'	# start Shiny interface
#'	launchApp(highlight.item='period.chart')	
#' }
#' 
#' 
#' 
#' @import shiny
#' @importFrom grDevices dev.off pdf
#'
#' @name rtsviz.period.chart
#' @docType package
#' 
NULL